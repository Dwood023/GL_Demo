#include "ModelManager.h"
#include "Model.h"


constexpr int MAX_MODELS = 200;

ModelManager::ModelManager()
{
	loaded_models.reserve(MAX_MODELS);

	std::vector<Vertex> cube_vertices = {
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),

		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),

		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),

		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),

		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),

		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f, -0.5)
	};

	loaded_models["default"] = Model(cube_vertices);
}


ModelManager::~ModelManager()
{
}

const Model& ModelManager::load_model(std::string name)
{
	auto search = loaded_models.find(name);
	if (search != loaded_models.end())
		return loaded_models[name];
	else
		throw std::out_of_range("Couldn't locate model: " + name);
	
	// TODO -- If not found, try to load from filesystem
	// If that fails, just throw the exception

	// TODO: insert return statement here
}
