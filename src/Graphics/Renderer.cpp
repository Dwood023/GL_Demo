#define GLEW_STATIC
#include <GLEW/glew.h>

#include "Renderer.h"
#include "Mesh.h"
#include "Shader.h"
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GLFW\glfw3.h>


constexpr int MAX_MODELS = 200;

void Renderer::init()
{
	glewExperimental = GL_TRUE;
	glewInit();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

/* Remember to init() before making a Renderer */
Renderer::Renderer()
	:shader("basic")
{
	shader.use();
	loaded_models.reserve(MAX_MODELS);
}

Renderer::~Renderer()
{
	// Free GPU data when Renderer dies (basically when program terminates)
	// TODO -- destroy loaded_models when no references remain, impl ref counting
	for (RenderModel& model : loaded_models)
		model.destroy();
}


std::vector<float> convert_mesh(const Model& model) {
	std::vector<float> vertex_data;

	for (Vertex vertex : model.vertices) {
		vertex_data.push_back(vertex.pos.x);
		vertex_data.push_back(vertex.pos.y);
		vertex_data.push_back(vertex.pos.z);
	}

	return vertex_data;
}

/* 
	Generates buffer for Model
	returns reference to RenderModel for that buffer 
	TODO -- If Model has already been loaded, don't generate a new RenderModel
*/
const RenderModel& Renderer::get_render_model(const Model& model)
{
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	std::vector<float> vertex_data = convert_mesh(model);
	size_t size = sizeof(vertex_data[0]) * vertex_data.size();
	glBufferData(GL_ARRAY_BUFFER, size, &vertex_data[0], GL_STATIC_DRAW);

	GLuint attrib_pos = glGetAttribLocation(shader.get_program(), "position");
	glVertexAttribPointer(attrib_pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(attrib_pos);

	// DESIGN -- Unbind things you're not using??
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	loaded_models.push_back(RenderModel(VAO, VBO));

	return loaded_models.back();
}

void Renderer::draw(std::vector<Mesh> meshes, Camera camera)
{
	set_view(camera);
	glClear(GL_COLOR_BUFFER_BIT);

	for (Mesh mesh : meshes) {
		// World transform is a uniform that needs setting before each draw
		glm::mat4 model = glm::translate(glm::mat4(1.f), mesh.position);
		shader.set_model_uniform(model);
		shader.use();

		mesh.draw();

		shader.set_model_uniform(glm::mat4(1.f)); // Reset model
	}
}

void Renderer::set_view(Camera camera)
{

	glm::vec3 up(0, 1.f, 0);

	view = glm::lookAt(camera.move_comp.position, camera.move_comp.get_target(), up);

	shader.set_view_uniform(view);

	// Camera settings
	const float fov = glm::radians(45.f);
	const float near = 0.1f;
	const float far = 100.f;
	float aspect_ratio = 1000 / 800; 
	projection = glm::perspective(fov , aspect_ratio, near, far);
	shader.set_proj_uniform(projection);
}
