#include "GLEW/glew.h"
#include "RenderModel.h"

#include <iostream>

RenderModel::RenderModel(GLuint VAO, GLuint VBO)
	:VAO(VAO)
	,VBO(VBO)
{
}

/* 
	How to delete VBO & VAO when not in use?
	Maybe Renderer should just check ref count and 
	do this itself?
*/
RenderModel::~RenderModel()
{
}

void RenderModel::draw() const 
{
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

void RenderModel::destroy()
{
	std::cout << "Deleting VAO: " << (int) VAO << std::endl;
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
}
