#include <iostream>
#include <fstream>
#include <string>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Shader.h"

/* 
	Standard is for vertex/fragment pair to have same name and stored in .\shaders
*/
Shader::Shader(std::string shader_name)
{
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_src = load_from_file(shader_name + ".vs");
	compile_shader(vertex_shader, vertex_src);

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_src = load_from_file(shader_name + ".fs");
	compile_shader(fragment_shader, fragment_src);

	program = glCreateProgram();
	glAttachShader(program, vertex_shader);
	glAttachShader(program, fragment_shader);
	glBindFragDataLocation(program, 0, "outColor");
	// TODO -- glGetProgramiv() for link error checking
	glLinkProgram(program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	// Do I need to do this every frame??
	glUseProgram(program);
}

Shader::~Shader()
{
    glDeleteProgram(program);
}

GLuint Shader::get_program() const
{
	return program;
}

void Shader::use()
{
	glUseProgram(program);
}

std::string Shader::load_from_file(std::string filename)
{
	std::string path = "shaders\\" + filename;
	std::ifstream file(path);

	if (file) {
		  return std::string(
			  std::istreambuf_iterator<char>(file),
			  std::istreambuf_iterator<char>() 
		  );
	}

	// TODO -- Error handling
	std::cerr << "Couldn't locate shader at " + path << std::endl;
	abort();
}

void Shader::compile_shader(GLuint shader, std::string src) {

	const char * c_src = src.c_str();

	glShaderSource(shader, 1, &c_src, NULL);
	glCompileShader(shader);

	// TODO -- Better error handling
	GLint result;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &result);

	// TODO -- glGetShaderInfoLog() for error reporting
	if (result != GL_TRUE) 
		std::cerr << "Compilation failed for shader" << std::endl;
	
}

void Shader::setBool(const std::string &name, bool value) const
{         
    glUniform1i(glGetUniformLocation(program, name.c_str()), (int)value); 
}
void Shader::setInt(const std::string &name, int value) const
{ 
    glUniform1i(glGetUniformLocation(program, name.c_str()), value); 
}
void Shader::setFloat(const std::string &name, float value) const
{ 
    glUniform1f(glGetUniformLocation(program, name.c_str()), value); 
}

void Shader::set_model_uniform(glm::mat4 model)
{
	glUniformMatrix4fv(glGetUniformLocation(program, "model"), 1, GL_FALSE, glm::value_ptr(model));
}

void Shader::set_view_uniform(glm::mat4 view)
{
	glUniformMatrix4fv(glGetUniformLocation(program, "view"), 1, GL_FALSE, glm::value_ptr(view));
}

void Shader::set_proj_uniform(glm::mat4 proj)
{
	glUniformMatrix4fv(glGetUniformLocation(program, "projection"), 1, GL_FALSE, glm::value_ptr(proj));
}

