#include <GLEW\glew.h>
#include <GLFW\glfw3.h>

#include <glm\glm.hpp>

#include "Mesh.h"
#include "Camera.h"
#include "Renderer.h"
#include "ModelManager.h"
#include "InputManager.h"

int main() {

	/* Window Creation */
	glfwInit();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	const int height = 800;
	const int width = 1000;

	GLFWwindow* window = glfwCreateWindow(width, height, "GL_Demo", nullptr, nullptr);

	if (!window) {
		printf("Context creation failed!!");
		return -1;
	}

	glfwMakeContextCurrent(window);
	float last_frame = glfwGetTime();

	glfwSetCursorEnterCallback(window, InputManager::cursor_entered);
	glfwSetCursorPosCallback(window, InputManager::cursor_moved);
	glfwSetMouseButtonCallback(window, InputManager::mouse_button_pressed);
	glfwSetKeyCallback(window, InputManager::key_pressed);

	Renderer::init();
	Renderer gl;

	ModelManager model_mgr{};

	std::vector<Mesh> world;
	world.push_back(Mesh(model_mgr.load_model("default"), gl.get_render_model(model_mgr.load_model("default")), glm::vec3(1.0f, 1.0f, 1.0f)));
	world.push_back(Mesh(model_mgr.load_model("default"), gl.get_render_model(model_mgr.load_model("default")), glm::vec3(0)));

	Camera camera(glm::vec3(0, 0, -3.f), glm::vec3(0,0,0));

	InputManager::assume_control(camera);

	// Event loop
	while (!glfwWindowShouldClose(window)) {

		const float current_frame = glfwGetTime();
		const float d_time = current_frame - last_frame; 
		last_frame = glfwGetTime();

		camera.update(d_time);
		gl.set_view(camera);
		gl.draw(world, camera);

		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	// Cleanup

	glfwTerminate();

	return 0;
}