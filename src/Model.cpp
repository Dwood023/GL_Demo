#include "Model.h"

Model::Model()
{
	// TODO -- Pass vertices into function
	// TODO -- Import 3D models
	vertices = {
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),

		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),

		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),

		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),

		Vertex(-0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f, -0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(0.5f, -0.5f,  0.5f),
		Vertex(-0.5f, -0.5f,  0.5f),
		Vertex(-0.5f, -0.5f, -0.5f),

		Vertex(-0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f, -0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f,  0.5f),
		Vertex(-0.5f,  0.5f, -0.5)
	};
}

Model::Model(std::vector<Vertex> vertices)
	:vertices(vertices)
{
}


Model::~Model()
{
}
