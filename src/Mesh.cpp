#include "Mesh.h"
#include "RenderModel.h"

Mesh::Mesh(const Model& model, const RenderModel& render_model, glm::vec3 world_pos)
	:model(model)
	,render_model(render_model)
	,position(world_pos)
{
}

Mesh::~Mesh()
{
}

void Mesh::draw()
{
	render_model.draw();
}
