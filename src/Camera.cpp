#include "Camera.h"

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

/* 
	Cameras store perspective and view matrices
	Right now, they also give them to the vertex shader as uniforms

	DESIGN -- Should classes like these change OpenGL state directly??
	TODO -- Change camera settings at runtime
*/

Camera::Camera(glm::vec3 position, Rotator rotation)
	:move_comp(position, rotation)
{
}

Camera::~Camera()
{
}

void Camera::update(float d_time)
{
	move_comp.update(d_time);
}

