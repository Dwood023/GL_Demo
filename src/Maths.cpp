#define GLM_ENABLE_EXPERIMENTAL
#include <glm\glm.hpp>
#include "Maths.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>

Rotator::Rotator(glm::vec3 euler_angles)
	:pitch(euler_angles.x)
	,yaw(euler_angles.y)
	,roll(euler_angles.z)
{
}

Rotator Rotator::operator+(const Rotator & b)
{
	float new_pitch = pitch + b.pitch;
	float new_yaw = yaw + b.yaw;
	float new_roll = roll + b.roll;
	return Rotator(glm::vec3(new_pitch, new_yaw, new_roll));
}

glm::vec3 Rotator::get_forward_vector() const
{
	glm::vec3 forward = glm::vec3(0, 0, 1.f);
	glm::vec3 heading = rotate_vector(forward);

	return glm::normalize(heading);
}

glm::vec3 Rotator::rotate_vector(glm::vec3 vector) const
{
	glm::mat4 rot = glm::eulerAngleYXZ(glm::radians(yaw), glm::radians(pitch), glm::radians(roll));
	return rot * glm::vec4(vector, 1.f);
}

