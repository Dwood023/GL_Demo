#include "MoveComp.h"




MoveComp::MoveComp(glm::vec3 position, Rotator direction)
	:position(position)
	,orientation(direction)
	,move_locks(0)
{
}

MoveComp::~MoveComp()
{
}

void MoveComp::add_move(glm::vec3 direction)
{
	move_locks += glm::normalize(direction);
}

void MoveComp::rotate(Rotator rot)
{
	orientation = orientation + rot;
}

glm::vec3 MoveComp::get_target() const
{
	return position + orientation.get_forward_vector();
}

void MoveComp::update(float d_time)
{
	position += (orientation.rotate_vector(move_locks) * move_speed * d_time);
}
