#include "InputManager.h"
#include <iostream>


void InputManager::assume_control(Camera& camera)
{
	active_camera = &camera.move_comp;
}

void InputManager::update(GLFWwindow* window, Camera& camera)
{
}

// TODO -- Rebindable mouse movement
/*
	Don't do this if cursor has been out of frame or disabled
	This is getting called before mouse button or entered callbacks
*/
void InputManager::cursor_moved(GLFWwindow* window, double x_pos, double y_pos) 
{
	if (!active_camera) return;
	if (!camera_rotation_mode) return;

	double mouse_sensitivity = 0.05f;

	double delta_yaw = x_pos - mouse_x_pos;
	double delta_pitch = y_pos - mouse_y_pos;

	mouse_x_pos = x_pos;
	mouse_y_pos = y_pos;

	glm::vec3 angles(delta_pitch * mouse_sensitivity, -delta_yaw * mouse_sensitivity, 0);

	active_camera->rotate(Rotator(angles));

	if (active_camera->orientation.pitch > 89.f)
		active_camera->orientation.pitch = 89.f;
	if (active_camera->orientation.pitch < -89.f)
		active_camera->orientation.pitch = -89.f;
}


void InputManager::key_pressed(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (key == GLFW_KEY_W) {
		if (action == GLFW_PRESS)
			active_camera->add_move(FORWARD);
		else if (action == GLFW_RELEASE)
			active_camera->add_move(-FORWARD);
	}
	if (key == GLFW_KEY_A) {
		if (action == GLFW_PRESS)
			active_camera->add_move(LEFT);
		else if (action == GLFW_RELEASE)
			active_camera->add_move(-LEFT);
	}
	if (key == GLFW_KEY_S) {
		if (action == GLFW_PRESS)
			active_camera->add_move(BACKWARD);
		else if (action == GLFW_RELEASE)
			active_camera->add_move(-BACKWARD);
	}
	if (key == GLFW_KEY_D) {
		if (action == GLFW_PRESS)
			active_camera->add_move(RIGHT);
		else if (action == GLFW_RELEASE)
			active_camera->add_move(-RIGHT);
	}


}

void InputManager::cursor_entered(GLFWwindow * window, int entered)
{
	if (entered) {
		glfwGetCursorPos(window, &mouse_x_pos, &mouse_y_pos);
	}
}

void InputManager::mouse_button_pressed(GLFWwindow * window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_RIGHT) {
		if (action == GLFW_PRESS) {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			glfwGetCursorPos(window, &mouse_x_pos, &mouse_y_pos);
			camera_rotation_mode = true;
		}
		else {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			glfwGetCursorPos(window, &mouse_x_pos, &mouse_y_pos);
			camera_rotation_mode = false;
		}
	}
}