#pragma once

#include <unordered_map>

#include "Model.h"

class ModelManager
{
public:
	ModelManager();
	~ModelManager();

	const Model& load_model(std::string name);
private:
	std::unordered_map<std::string, Model> loaded_models;
};

