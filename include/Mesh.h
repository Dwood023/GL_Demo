#pragma once

#include <glm/glm.hpp>

#include "Model.h"
#include "RenderModel.h"

/*
	Mesh is a renderable instance of a Model
*/
class Mesh
{
public:
	Mesh(const Model& model, const RenderModel& render_model, glm::vec3 world_pos);
	~Mesh();

	const Model& model;
	void draw();

	glm::vec3 position;

private:
	const RenderModel& render_model;

};


