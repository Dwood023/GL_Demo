#pragma once

#include <GLEW\glew.h>

class RenderModel
{
public:
	RenderModel(GLuint VAO, GLuint VBO);
	~RenderModel();
	void draw() const;
	void destroy();
private:
	const GLuint VAO;
	const GLuint VBO;
};

