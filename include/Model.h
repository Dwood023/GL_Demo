#pragma once
#include <vector>

#include "Vertex.h"
class Model
{
public:
	Model();
	Model(std::vector<Vertex> vertices);
	~Model();
	std::vector<Vertex> vertices;
private:
};

