#pragma once
#include <glm/glm.hpp>

class Rotator {
public:
	Rotator(glm::vec3 euler_angles);
	Rotator operator+(const Rotator& b);

	glm::vec3 get_forward_vector() const;
	glm::vec3 rotate_vector(glm::vec3 vector) const;
	float pitch; // x
	float yaw; // Y
	float roll; // Z
};


