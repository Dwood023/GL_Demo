#pragma once
#include <GLFW\glfw3.h>
#include <vector>

#include "Camera.h"

typedef unsigned int keycode;
/*
	Polls for key presses and updates camera or player
	Which camera or player?

	-> Passed in every update()?
	-> InputManager stores ref to controlled_player & controlled_camera
*/
namespace InputManager
{
	void assume_control(Camera& active_camera);
	void update(GLFWwindow* window, Camera& camera);


	void cursor_moved(GLFWwindow* window, double x_pos, double y_pos);
	void key_pressed(GLFWwindow* window, int key, int scancode, int action, int mods);
	void cursor_entered(GLFWwindow* window, int entered);
	void mouse_button_pressed(GLFWwindow* window, int button, int action, int mods);

	namespace {
		MoveComp* active_camera;
		bool camera_rotation_mode = false;
		double mouse_x_pos = 0.f;
		double mouse_y_pos = 0.f;

	}

};
