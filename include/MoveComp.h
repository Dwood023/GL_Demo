#pragma once

#include <glm/glm.hpp>
#include "Maths.h"

constexpr glm::vec3 FORWARD(0, 0, 1.f);
constexpr glm::vec3 BACKWARD(0, 0, -1.f);
constexpr glm::vec3 RIGHT(-1.f, 0, 0);
constexpr glm::vec3 LEFT(1.f, 0, 0);
constexpr glm::vec3 UP(0, 1.f, 0);
constexpr glm::vec3 DOWN(0, 1.f, 0);

class MoveComp
{
public:
	MoveComp(glm::vec3 position, Rotator direction);
	~MoveComp();

	/* World Position */
	glm::vec3 position;
	/* World rotation */
	Rotator orientation;

	void add_move(glm::vec3 direction);
	void rotate(Rotator rot);
	glm::vec3 get_target() const;
	void update(float d_time);
private:
	float move_speed = 5.f;

	glm::vec3 move_locks;
};

