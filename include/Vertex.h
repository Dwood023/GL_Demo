#pragma once

#include <glm/glm.hpp>

class Vertex
{

public:
	Vertex(float x, float y, float z);
	glm::vec3 pos;

private:

};