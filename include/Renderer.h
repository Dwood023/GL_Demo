#pragma once

#include <GLEW\glew.h>

#include <vector>

#include <glm/glm.hpp>

#include "Mesh.h"
#include "Model.h"
#include "Camera.h"
#include "Shader.h"
#include "RenderModel.h"

class Renderer
{
public:
	Renderer();
	~Renderer();
	const RenderModel& get_render_model(const Model& model);
	void draw(std::vector<Mesh> meshes, Camera camera);
	void set_view(Camera camera);
	static void init();

private:

	std::vector<RenderModel> loaded_models;

	Shader shader;

	glm::mat4 view;
	glm::mat4 projection;
};

