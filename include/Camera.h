#pragma once

#include <glm/glm.hpp>
#include "Maths.h"
#include "MoveComp.h"

class Camera
{
public:
	Camera(glm::vec3 position, Rotator direction);
	~Camera();

	MoveComp move_comp;
	void update(float d_time);

private:
};

