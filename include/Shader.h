#pragma once

#include <GLEW\glew.h>
#include <glm/glm.hpp>

class Shader
{
public:
	Shader(std::string shader_name);
	~Shader();

	// TODO -- Better way of getting attribute locations to meshes
	GLuint get_program() const;
	void use();

	
	// Helpers for setting uniforms
	void setBool(const std::string &name, bool value) const;
	void setInt(const std::string &name, int value) const;
	void setFloat(const std::string &name, float value) const;

	void set_model_uniform(glm::mat4 model);
	void set_view_uniform(glm::mat4 view);
	void set_proj_uniform(glm::mat4 proj);

private:

	GLuint program;

	static std::string load_from_file(std::string filename);
	void compile_shader(GLuint shader, std::string filename);
	
};

